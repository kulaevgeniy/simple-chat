const io = require('socket.io');
const express = require('express');
const http = require('http');
const redis = require("redis");
const app = express();
const server = http.createServer(app);
const socket = io.listen(server);

const redisHost  = process.env.REDIS_HOST ? process.env.REDIS_HOST : 'localhost';
const redisPort  = process.env.REDIS_PORT ? process.env.REDIS_PORT : 6379;
const socketPort = process.env.SOCKET_PORT ? process.env.SOCKET_PORT : 1337;

const subscriber   = redis.createClient({
    host: redisHost,
    port: redisPort
});


server.listen(socketPort);

subscriber.on("message", (channel, message) => {
    console.log("Message '" + message + "' arrived!");
    socket.emit('message', JSON.parse(message));
});

subscriber.subscribe("message");
