
const express        = require('express');
const router         = express.Router();
const MessageService = require('./../services/MessageService');

router.get('/', function(req, res) {
  MessageService.getList(req.query.limit, req.query.offset).then(function (messages) {
      return res.send(messages);
  }).catch(err => {
      return res.status(500).send({err});
  });
});

router.post('/', function(req, res) {
  if (!req.body.message || !req.body.user) {
      return res.status(500).send('Not all required parameters');
  }
  MessageService.add(req.body.message, req.body.user).then(function (message) {
      return res.send(message);
  }).catch(err => {
      return res.status(500).send({err});
  });
});

module.exports = router;
