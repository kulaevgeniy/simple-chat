
const express     = require('express');
const router      = express.Router();
const UserService = require('./../services/UserService');

router.post('/', function(req, res, next) {
  if (!req.body.nickname) {
      return res.status(500).send('Nickname is required');
  }

  UserService.findOrCreate(req.body.nickname).then((user) => {
      return res.send({id: user._id.toString()});
  }).catch(err => {
      return res.status(500).send({err});
  });
});

module.exports = router;
