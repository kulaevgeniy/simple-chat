#!/usr/bin/env node

const app     = require('../app');
const http    = require('http');
const cluster = require('cluster');
const os      = require('os');
const port    = process.env.PORT ? process.env.PORT : 8080;

if(cluster.isMaster) {
    let numWorkers = os.cpus().length;

    console.log(`Master cluster setting up ${numWorkers} sub-clusters...`);

    for(let i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        console.log(`Sub-cluster ${worker.process.pid} is online`);
    });

    cluster.on('exit', function(worker, code, signal) {
        console.log(`Sub-cluster ${worker.process.pid} died with code: ${code}, and signal: ${signal}`);
        console.log('Starting a new worker');
        cluster.fork();
    });
} else {
    const server = http.createServer(app);

    app.set('port', port);
    server.listen(port);
}