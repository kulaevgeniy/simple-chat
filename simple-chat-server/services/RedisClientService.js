
const redis     = require("redis");
const redisHost = process.env.REDIS_HOST ? process.env.REDIS_HOST : 'localhost';
const redisPort = process.env.REDIS_PORT ? process.env.REDIS_PORT : 6379;

class RedisClientService {
    constructor() {
        this.publusher = redis.createClient({
            host: redisHost,
            port: redisPort
        });
    }

    getPublisher() {
        return this.publusher;
    }
}

module.exports = new RedisClientService();