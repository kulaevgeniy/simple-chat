const Promise            = require('promise');
const DbService          = require('./../services/DbService');
const UserService        = require('./../services/UserService');
const ObjectID           = require('mongodb').ObjectID;
const RedisClientService = require('./../services/RedisClientService');

class MessageService {
    getList(limit = 20, offset = 0) {
        return new Promise((resolve, reject) => {
            let messageCollection = DbService.getCollection('Message');

            messageCollection
                .find()
                .limit(parseInt(limit))
                .skip(parseInt(offset))
                .sort('date', 'desc')
                .toArray((err, messages) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(messages);
                }
            );
        });
    }

    add(message, userId) {
        let me = this;

        return new Promise((resolve, reject) => {
            UserService.get(userId).then(user => {
                let messageCollection = DbService.getCollection('Message');

                return messageCollection.insertOne({
                    body: message,
                    user: user,
                    date: new Date()
                }, (err, record) => {
                    if (err) {
                        return reject(err);
                    }
                    me.get(record.insertedId.toString())
                        .then((message) => {
                            resolve(message);
                            RedisClientService.getPublisher().publish("message", JSON.stringify(message));
                        }
                    );
                });
            }).catch(err => {
                return reject(err);
            });
        });
    }

    get(id) {
        return new Promise((resolve, reject) => {
            let messageCollection = DbService.getCollection('Message');

            messageCollection.findOne(ObjectID(id), (err, message) => {
                if (err) {
                    return reject(err);
                }
                if (!message) {
                    return reject('Message doesn\'t exist');
                }

                resolve(message);
            });
        });
    }
}

module.exports = new MessageService();