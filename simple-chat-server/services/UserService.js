const Promise   = require('promise');
const DbService = require('./../services/DbService');
const mongodb   = require('mongodb');

class UserService {
    findOrCreate(nickname) {
        let me = this;

        return new Promise((resolve, reject) => {
            let userCollection = DbService.getCollection('User');

            userCollection.findOne({'nickname': nickname}, (err, user) => {
                if (err) {
                    reject(err);
                }

                if (!user) {
                    return userCollection.insertOne({
                        'nickname': nickname
                    }, (err, record) => {
                        if (err) {
                            reject(err);
                        }
                        me.get(record.insertedId.toString()).then((user) => {
                            resolve(user);
                        });
                    });
                } else {
                    resolve(user);
                }
            });
        });
    }

    get(id) {
        return new Promise((resolve, reject) => {
            let userCollection = DbService.getCollection('User');

            userCollection.findOne(mongodb.ObjectID(id), (err, user) => {
                if (err) {
                    return reject(err);
                }
                if (!user) {
                    return reject('User doesn\'t exist');
                }

                return resolve(user);
            });
        });
    }
}

module.exports = new UserService();