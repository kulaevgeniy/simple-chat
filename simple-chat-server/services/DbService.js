
const MongoClient = require('mongodb').MongoClient;

const host = process.env.DB_HOST ? process.env.DB_HOST : 'localhost';
const port = process.env.DB_PORT ? process.env.DB_PORT : '27017';
const connectionUrl = `mongodb://${host}:${port}/simple-chat`;

class DbService {

    constructor() {
        let me = this;

        if (this.database) {
            return ;
        }

        MongoClient.connect(connectionUrl, (err, db) => {
            if (err) {
                console.log('Unable to connect to Mongodb.');
            }

            me.database = db;
        });
    }

    getCollection(name) {
        return this.database.collection(name);
    }
}

module.exports = new DbService();