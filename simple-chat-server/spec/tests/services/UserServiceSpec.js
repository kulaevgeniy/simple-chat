
const UserService = require('../../../services/UserService');
const DbService   = require('../../../services/DbService');
const mongo       = require('mongodb');

describe('UserService', () => {

    describe('try to find user', () => {
        let collectionMock;
        let userMock = {nickname: 'Aaron'};

        beforeEach(() => {
            collectionMock = jasmine.createSpyObj('collection', ['findOne', 'insertOne']);
            spyOn(DbService, 'getCollection').and.returnValue(collectionMock);
        });

        afterEach(() => {
            expect(DbService.getCollection).toHaveBeenCalledWith('User');
            expect(collectionMock.findOne).toHaveBeenCalled();
        });

        it ('and found', () => {
            collectionMock.findOne.and.callFake((object, callback) => {
                expect(object).toEqual({nickname: userMock.nickname});
                callback(false, userMock)
            });

            UserService.findOrCreate(userMock.nickname).catch(() => {
                fail(`User with nickname "${userMock.nickname}" should be returned`);
            });
        });

        it ('not found and created', () => {
            collectionMock.findOne.and.callFake((object, callback) => {
                expect(object).toEqual({nickname: userMock.nickname});
                callback(false, false)
            });
            collectionMock.insertOne.and.callFake((object, callback) => {
                expect(object).toEqual({nickname: userMock.nickname});
                callback(false, userMock)
            });
            collectionMock.findOne.and.callFake((object, callback) => {
                callback(false, userMock);
            });

            UserService.findOrCreate(userMock.nickname).catch(() => {
                fail(`User with nickname "${userMock.nickname}" should be created`);
            });
        });

        it('with db connection problem', () => {
            let err = 'Something wrong wit database connection';

            collectionMock.findOne.and.callFake((object, callback) => {
                callback(err);
            });

            UserService.get(1).then(function () {
                fail('User should not be returned');
            }).catch(error => {
                expect(error).toEqual(err)
            });
        });
    });

    describe('try to get user by id', () => {
        let collectionMock;
        let ObjectIDMock;
        let userMock = {id: 1};

        beforeEach(() => {
            collectionMock = jasmine.createSpyObj('collection', ['findOne']);
            ObjectIDMock   = mongo.ObjectID(1);

            spyOn(DbService, 'getCollection').and.returnValue(collectionMock);
            spyOn(mongo, 'ObjectID').and.returnValue(ObjectIDMock);
        });

        afterEach(() => {
            expect(DbService.getCollection).toHaveBeenCalledWith('User');
            expect(mongo.ObjectID).toHaveBeenCalledWith(1);
            expect(collectionMock.findOne).toHaveBeenCalled();
        });

        it('should return user', () => {
            collectionMock.findOne.and.callFake((object, callback) => {
                callback(false, userMock);
            });

            UserService.get(1).then((user) => {
                expect(user).toEqual(userMock)
            }).catch(function () {
                fail('User with #1 should be returned');
            });
        });

        it('should not return user', () => {
            collectionMock.findOne.and.callFake((object, callback) => {
                callback(false, false);
            });

            UserService.get(1).then(function () {
                fail('User should not be returned');
            }).catch(error => {
                expect(error).toEqual('User doesn\'t exist')
            });
        });

        it('with db connection problem', () => {
            let err = 'Something wrong wit database connection';

            collectionMock.findOne.and.callFake((object, callback) => {
                callback(err);
            });

            UserService.get(1).then(function () {
                fail('User should not be returned');
            }).catch(error => {
                expect(error).toEqual(err)
            });
        });
    });
});