
const request = require('request');
const port    = process.env.PORT ? process.env.PORT : 8080;
const baseUrl = `http://localhost:${port}`;

describe('Join routes', () => {
    describe('POST /join', () => {
        let id;

        it('successfully registered', () => {
            request.post(
                `${baseUrl}/join`,
                {
                    form: {
                        nickname: 'Aaron'
                    }
                },
                function(error, response, body) {
                    expect(response.statusCode).toEqual(200);
                    id = body.id;
                }
            );
        });

        it('successfully fetch', () => {
            request.post(
                `${baseUrl}/join`,
                {
                    form: {
                        nickname: 'Aaron'
                    }
                    },
                function(error, response, body) {
                    expect(response.statusCode).toEqual(200);
                    expect(body.id).toEqual(id);
                }
            );
        });

        it('not all required parameters', () => {
            request.post(
                `${baseUrl}/join`,
                {},
                function(error, response, body) {
                    expect(response.statusCode).toEqual(500);
                    expect(body).toEqual('Nickname is required');
                }
            );
        });
    });
});