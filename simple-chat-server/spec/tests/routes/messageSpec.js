
const request     = require('request');
const port        = process.env.PORT ? process.env.PORT : 8080;
const baseUrl     = `http://localhost:${port}`;
const UserService = require('../../../services/UserService');

describe('Message routes', () => {
    describe('GET /messages', () => {
        it('successfully received messages list', () => {
            request.get(
                `${baseUrl}/messages`,
                function(error, response, body) {
                    expect(response.statusCode).toEqual(200);
                }
            );
        });
    });

    describe('POST /messages', () => {
        it('successfully created message', () => {
            UserService.findOrCreate('UserName').then((user) => {
                request.post(
                    `${baseUrl}/messages`,
                    {
                        form: {
                            message: 'Text',
                            user: user._id.toString()
                        }
                    },
                    function(error, response, body) {
                        let msg = JSON.parse(body);
                        expect(response.statusCode).toEqual(200);
                        expect(msg.body).toEqual('Text');
                        expect(msg.user.nickname).toEqual('UserName');
                    }
                );
            });

        });

        it('user not found', () => {
            request.post(
                `${baseUrl}/messages`,
                {},
                function(error, response, body) {
                    expect(response.statusCode).toEqual(500);
                    expect(body).toEqual('Not all required parameters');
                }
            );
        });
    });
});