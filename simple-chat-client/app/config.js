'use strict';

angular
    .module('chatApp')
    .config(appConfig);

appConfig.$inject = ['webSocketProvider', '$routeProvider', 'AppConfig'];

function appConfig(webSocketProvider, $routeProvider, AppConfig) {
    $routeProvider.otherwise({redirectTo: '/join'});

    webSocketProvider.configure(AppConfig.socketServerUrl);
}
