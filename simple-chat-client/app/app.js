'use strict';

// Declare app level module which depends on views, and components
angular.module('chatApp', [
  'luegg.directives',
  'ngRoute'
]).run(runBlock);

runBlock.$inject = ['$rootScope', '$location', 'webSocket'];

function runBlock($rootScope, $location, webSocket) {
    $rootScope.$on("$routeChangeStart", function () {
        let userId = localStorage.getItem('userId');

        if (!userId) {
            $location.path('/join');
        }

        webSocket.connect();
    });
}
