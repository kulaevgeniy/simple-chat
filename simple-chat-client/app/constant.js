'use strict';

angular
    .module('chatApp')
    .constant('AppConfig', {
        serverUrl: 'http://localhost:8080',
        socketServerUrl: 'http://localhost:1337'
    });