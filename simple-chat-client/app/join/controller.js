'use strict';

angular.module('chatApp').controller('JoinController', JoinController);

JoinController.$inject = ['$location', '$http', 'AppConfig'];

function JoinController($location, $http, AppConfig) {
    let vm = this;

    vm.nickname = null;

    vm.join = join;

    function join() {
        return $http.post(
            AppConfig.serverUrl + '/join',
            {
                nickname: vm.nickname
            }
        ).then(response => {
            if (response.data.id) {
                localStorage.setItem('userId', response.data.id);
                $location.path('/chat');
            }
        });
    }
}