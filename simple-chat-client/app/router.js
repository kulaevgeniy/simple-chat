'use strict';

angular.module('chatApp').config(routeConfig);

routeConfig.$inject = ['$routeProvider'];

function routeConfig($routeProvider) {
    $routeProvider
        .when('/join', {
            templateUrl: 'join/view.html',
            controller: 'JoinController',
            controllerAs: 'vm'
        })
        .when('/chat', {
            templateUrl: 'chat/view.html',
            controller: 'ChatController',
            controllerAs: 'vm'
        });
}