
'use strict';

angular
    .module('chatApp')
    .provider('webSocket', webSocketProvider);

function webSocketProvider() {
    let url = '';

    this.configure = function(urlArg) {
        if (urlArg) {
            url = urlArg;
        }
    };

    this.$get = Socket;

    function Socket() {
        let service = {
            connect: connect,
        };

        return service;

        function connect() {
            service.socket = io.connect(url);

        }
    }
}
