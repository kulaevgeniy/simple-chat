'use strict';

angular.module('chatApp').controller('ChatController', ChatController);

ChatController.$inject = [
    '$http',
    'AppConfig',
    '$scope',
    'webSocket',
    '$location'
];

function ChatController($http, AppConfig, $scope, webSocket, $location) {
    let vm = this;

    vm.messageList = [];
    vm.message = null;
    vm.limit  = 20;
    vm.offset = 0;
    vm.listEnd = false;

    vm.send = send;

    init();

    function init() {
        let el = document.querySelector('.message-container');

        angular.element(el).bind('scroll', () => {
            if (el.scrollTop === 0 && !vm.listEnd) {
                el.scrollTop = 20;
                vm.limit = 5;
                loadList();
            }
        });

        loadList();

        initSocketListener();
    }

    function loadList() {
        $http.get(
            AppConfig.serverUrl + '/messages?offset=' + vm.offset + '&limit=' + vm.limit
        ).then(response => {
            vm.messageList = vm.messageList.concat(response.data);
            vm.offset += vm.limit;
            if (response.data.length < vm.limit){
                vm.listEnd = true;
            }

        });
    }

    function send() {
        $http.post(
            AppConfig.serverUrl + '/messages',
            {
                message: vm.message,
                user: localStorage.getItem('userId')
            }
        ).then(() => {
            vm.message = null;
        }).catch(() => {
            localStorage.removeItem('userId');
            $location.path('/join');
        });
    }

    function initSocketListener() {
        webSocket.socket.on('message', message => {
            $scope.$apply(() => {
                vm.messageList.push(message);
            });
        });
    }
}
